%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

%\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out
                                                          % if you need a4paper
\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                                                          % paper

\IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document



% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
NDFS: A multi disk aware file system
}

\author{Pallav Agarwal}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{listings}
\begin{document}
\maketitle
\thispagestyle{empty}
\pagestyle{empty}

\begin{abstract}
Speed and size are often at war when choosing a storage system. While many applications
can optimize themselves using RAM effectively, many still let the Disk I/O be the bottleneck factor in their performance. This paper presents NDFS (N-Disk File System),
a file system that can optimize itself to effectively use multiple underlying disks
similar to cache hierarchy. The aim of this paper is to provide a proof-of-concept that
a file system can manage data intelligently based on the underlying block device.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}
Solid State Drives keep popping up everywhere. Most cloud servers use SSDs exclusively
due to the order-of-magnitude difference in read and write speeds between HDDs and
SSDs. Combine this with flash storage, tmpfs (RAM), volatile and non-volatile storage,
suddenly, you have a lot of options to choose from. Unfortunately, SSDs still cost $\sim$8x
more than HDDs (as of Nov 2017). This makes it very hard to choose a Hard Drive for a 
personal computer, by having to compromise on either speed on disk size. While having
multiple disks is an option, for a mainstream user, manually managing data on each drive is not expected.

A file system is in a perfect position to abstract away the data management away from the
user. Like a LRU cache, the file system always has up-to-date information on which files 
or blocks are accessed more frequently than others, and which blocks are completely stale.
Most file systems employ re-ordering and caching to hide some of the performance drawbacks
that may be present due to a slow block device, however, the order-of-magnitude separation
still remain between RAM, SSDs and HDDs.

This paper presents a way to optimize file system storage without the need for specialized
hardware (hybrid disks etc.).

\section{N-DiskFS}
\subsection{Larger Scope}
\noindent While NDFS is a infant project, it serves as a proof of concept for the construction
of a file system that can identify and optimize storage for:
\begin{itemize}
\item Rotational/Non-Rotational disk drives
\item Volatile/Non-Volatile storage
\item Raid devices
\item Network file system
\end{itemize}

\noindent NDFS can also be built upon existing file systems, resulting in a filesystem that
can benefit from optimizations of the existing file system as well as the multi disk setup.

\subsection{Project Objectives}
\noindent The project was initially divided into 7 phases, each of which were accomplished during the 
semester.
\subsubsection{Minimal file system}
\noindent The project started with a very tiny implementation of a file system, with no support for
any actual file system operations. The aim for the minimal file system was to arrange the code in a modular way, and decide on the file system specification for future phases. This minimal file system was helped by the inital commits of simplefs\cite{simplefs}.

\subsubsection{Interaction with dm drivers}
\noindent Usually the file systems are restricted to one block device. Linux thus, does not have
a way to mount a file system consisting on multiple devices/partitions. This hiccup was overcome by
dm (device-mapper) that enables mapping of physical block devices into higher level block devices.
However, the file system needs access to the properties held by the underlying devices, and not just
the virtual block device.

This cannot be fixed (to the best of my knowledge) with an external module. For NDFS, the kernel was patched to export one crucial function related to the mapped devices so that it was accessible to the file system.

\subsubsection{Accessing device properties}
\noindent The underlying devices, once accessible, expose their properties via a number of structs.
While it is possible to determine \textit{ANY} property about the underlying device, the demo 
presented in the paper focuses on a single property : rotational/non-rotational.

The paper assumes that the rotational device refers to a spin hard drive, while the non-rotational
device refers to a solid state drive. As a result, the paper assumes that the solid state drive
should be prioritized for high availability files.

\subsubsection{Deciding on the file system structure}
\noindent The original idea was to have a simple allocation system where large files go to the HDD while the smaller files are kept on the SSD. However, the current version of N-DiskFS goes beyond
that. We introduce \textit{ndfs\_ladder}, a priority based ladder that a file/directory/inode/block
can climb or descend based on its own usage characteristics.

Other decisions made in NDFS are:
\begin{itemize}
\item There is a single inode store in the file system (limit on total number of files)
\item File blocks are accessed sequentially (slow for large files, and can't write directly to offset)
\item Bitmap for `dirty' value of blocks is stored at the beginning of the file system.
\end{itemize}

These decisions were made to simplify the construction of NDFS, and are \textit{NOT} compromises
related to the actual purpose of NDFS. The API introduced in this paper should be possible to
implement over (almost) any file system, irrespective of these decisions.

\subsubsection{Demo code corresponding to the allocation method}
For the default allocation, we decided to have the data start allocation on the highest priority
device. Things spill over to the next device (and next after that...) when the device in use gets
filled up.

This stage also required the implementation of a working basic file system (create, open, read,
write, mkdir, etc.) calls. Due to the lack of updated documentation, this took a rather long time.

\subsubsection{Exposing the NDFS API}
The NDFS API is exposed to the user in form of ioctl commands, so that user can override the 
allocation style of specific files if she wishes to. Presence of IOCTL also enables deterministic
testing, as well as demonstration of use of the file system.

\subsubsection{Cleanup}
Great care was taken throughout the project so that the code is cleaned up just before every commit, 
so that it is possible for an interested reader to go through the code - one commit at a time - to
properly understand how each and every part works together.\\
{\footnotesize{\url{https://github.com/pallavagarwal07/nDiskFS/commits}}}

\subsection{Challenges}
The major challenge during the construction of NDFS was the lack of available documentation. Linux 
kernel changes at a surprisingly fast pace, which means that every tutorial or documentation available
on the Internet was sadly out of date. The only viable way to understand the various functions and
APIs was to read the code for existing file systems in the kernel. A lot of time was spent pouring
over ext2 and debugfs codes included with the kernel source code.

Reading the code was not always enough, since none of the existing file systems have anything to
do with device mapper or its internal workings. Getting the underlying device properties from the
available virtual block device required a lot of guess work, tens of kernel panics and at least
a hundred VM reboots.

Thinking of a simple and elegant way to utilize the multiple disks, in a way that it doesn't add much
overhead to the existing mechanism of whatever file system it is implemented on top of also required
some thought.

Lastly, since the file system is a highly inter-twined piece of code, certain segments of code 
required tricky abstractions so that systems of allocation, management of free blocks, and read/write
of files could be handled separately. This is best illustrated by the fact that the switch from
a single block bit vector (for clean blocks) to arbitrary sized bit vector required touching only
the lines directly related to the block management (30-40 lines), and none of the other 1200 lines
of code.

\section{N-DiskFS Core Design}
We'll split the design into two parts: Base file system (could be switch for any other file system),
and NDFS specific parts.

\subsection{Base file system}
The base file system design is very simple. The file system contains blocks of size 4K each (can be changed).
The first few blocks are arranged as follows:

\subsubsection{Block 0: Superblock}
Superblock for NDFS contains the version, unique magic number, block size, number of inodes, number of bitmap blocks.

\subsubsection{Block 1: Inode Store}
Inode store sequentially contains inodes present in the file system. The pointer to $i^{th}$ offset 
within this block will point to the inode corresponding to inode number $i$.

\subsubsection{Block 2 - Block k: Bitmaps}
Bitmaps store one bit corresponding to every block on the device. The bit is one if the block is free,
and 0 if the corresponding block isn't. Thus, for a 1GB disk, there would be $\frac{1GB}{4KB} = 262144$ bits,
which is 32KB. Thus, for a 1GB disk, the bitmaps will take up 8 blocks.

\subsubsection{Block k+1 - End}
Data blocks. Data can correspond to either files or directories. For directories, the corresponding data
block contains the directory entries of its children. These directory entries are only allowed to take
one block.

For files, there is no such restriction. The file block is a struct of the form:
\begin{lstlisting}
struct ndfs_data {
    uint64_t prev_block;
    char data[NDFS_DATA_PER_BLOCK];
    uint64_t succ_block;
};
\end{lstlisting}

Thus, every block stores data that is a little less than the size of a block, and stores the block number of
the next and previous data block where the data corresponding to this file is stored.

\subsection{NDFS Ladder}
For using the underlying properties of devices, NDFS defines what it calls \textit{ndfs\_ladder}. Think of all layers (maps) in the virtual device as a rung of
the ladder.

\begin{figure}[h!!]
\includegraphics[width=0.4\textwidth]{ladder}
\caption{Sample priority ladder}
\end{figure}

When NDFS is mounted, it scans the drive to access all the underlying device ``strips''. For each strip, it creates an entry in the ladder with the start and end
block of that strip in the virtual block device. Also, for every strip, NDFS
calculates a priority value based on the device property. In the current 
implementation, only two priority values are given : 1 (non-rotational), and 2 (rotational). However the \textit{ndfs\_scan} function may be easily modified to
add more priority values (without changing rest of the code).

Once the rungs are known, NDFS sorts them according to the priority value. The block
allocation functions are modified such that instead of searching for the block in the
device, they search for the block in each rung, starting from top (highest priority)
to the bottom.

\textit{Note that right now we are not considering the effects of volatility/non-volatility. Some of the content may be inappropriate for volatile storage.}

Now, each block in the device belongs to one of the rungs of the ladder. For
optimization, NDFS provides two more APIs:

\begin{enumerate}
\item \textit{ndfs\_ascend}
\item \textit{ndfs\_descend}
\end{enumerate}

Both of these functions may be called on inodes or separate blocks. In case of inodes,
an ascend function will shift all the blocks belonging to inode to a higher priority
rung (when possible). Similarly, descend will shift all blocks belonging to inode to a 
lower priority rung.

A file system knows which files are accessed frequently. Thus, depending on the usage 
of a file, NDFS can call ascend or descend on the files. Like a splay tree, this is a self optimizing structure. The files would after some time organize themselves in a 
way that the most frequently accessed files are always on the top of the ladder and 
the stale files are on the bottom.

\section{Correctness}
\subsection{Known Bugs}
The following bugs are known:
\begin{itemize}
\item Race conditions:\\ The file system currently has no mutex locks. During rapid development, I was more 
scared of deadlocks than race conditions (doing operations synchronously works fine). This is easily fixable, given some time.
\item Limited number of files:\\ Due to a single inode store, file system can have maximum 128 files. Increasing the limit by a constant factor is fairly easy, but making it unbounded will require some thought.
\item Unsupported functions:\\ Some common (like rename/move) and most uncommon file system functions haven't
been implemented.
\item The absence of benchmarks or automated tests is due to the lack of time.
\end{itemize}

\subsection{Works (but beware)}
\noindent The following has been implemented.\\(experimentally tested, not heavily tested)
\begin{itemize}
\item Creation of files
\item Creation of directories
\item Writing to a file
\item Truncating a file
\item Removing a file/directory
\item Lookup in directory
\item Listing directory contents
\item ``Ascending'' and ``Descending'' a file
\item Writing large ($\sim$1000 blocks) files 
\item IOCTL functions to call ascend and descend manually.
\end{itemize}

\section{Future Work}
\subsection{More ladders!}
For binary features like volatility, the rungs can contain a tag so that the block can choose whether it
wants to shift to that rung. There needs to be a way to mark data that is safe to transfer to a volatile
storage area.

Similarly, there can be multiple ladders corresponding to different characteristics (throughput vs random access for instance), and data can move along the ladder that is more significant to that ladder. For example,
a graphical computer game would need more random access, while a program to compute millions of digits of
PI would prefer localized throughput.

\subsection{Integration with existing file systems}
We believe that the API is general enough that it can be adapted for most existing file systems. It would
be very beneficial to have a serious file system build over the NDFS file system.

For instance, a version of ext4 that could rearrange files over multiple disks would be very useful for
home desktops, since home desktop budgets aren't too high to buy a big SSD, and the storage requirement
still dictate a larger drive. A small SSD, and a large SSD combined could give huge performance benefits
in this case.

\section{Conclusion}
In this paper, we present N-DiskFS, a file system that can span multiple disks or disk partitions, including both physical and non-physical drives. We describe the concept of \textit{ndfs\_ladder} and \textit{priority rungs}. We describe how a file system can use its information of file usage to optimize the storage of the
mentioned files into better suited disks (or partitions). Finally, we offer an implementation of such a file system, which allows the files to be moved between underlying partitions at a block level.

\begin{thebibliography}{9}
\bibitem{simplefs} 
Sankar P.
\textit{Simple file system}. 
https://github.com/psankar/simplefs

\bibitem{tldp} 
TLDP
\textit{The Linux Kernel Programming Guide}\\
http://www.tldp.org/LDP/lkmpg/2.6/html/lkmpg.html
 
\bibitem{tut} 
kmu1990.
\textit{Writing a File System in Linux Kernel}\\
https://kukuruku.co/post/writing-a-file-system-in-linux-kernel/

\bibitem{source}
Linux Kernel Developers
\textit{Linux Kernel Source Code} \\
https://github.com/torvalds/linux
 
\end{thebibliography}

\end{document}
